import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { Observable} from 'rxjs';
import { tap } from 'rxjs/operators';

import { IOrder } from '../models/order';

@Injectable({
  providedIn: 'root'
})
export class CheckoutService {
  private apiUrl = 'api/orders';

  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' }),
  };

  constructor(
    private http: HttpClient,
  ) {}

  saveOrder(order: IOrder): Observable<IOrder> {
    return this.http.post<IOrder>(this.apiUrl, order, this.httpOptions)
      .pipe(
        tap(() => console.log("Save order")),
      );
  }
}
