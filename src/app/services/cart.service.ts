import { Injectable } from '@angular/core';

import { ICartItem } from '../models/cart';
import { IProduct } from '../models/product';

@Injectable({
  providedIn: 'root'
})
export class CartService {
  private cart: Record<IProduct['id'], ICartItem> = {}

  constructor() {}

  localStorageGetCart() {
    return JSON.parse(localStorage.getItem('cart') || '{}');
  }

  localStorageSetCart() {
    localStorage.setItem('cart', JSON.stringify(this.cart));
  }

  getItems() {
    this.cart = this.localStorageGetCart();
    let cartItems: ICartItem[] = []

    Object.entries(this.cart).forEach(([, val]) => {
      cartItems.push(val);
    })

    return cartItems;
  }

  addItem(product: IProduct, quantity: number) {
    this.cart = this.localStorageGetCart();

    if (product.id in this.cart) {
      let item = this.cart[product.id]
      item.quantity += quantity;
      item.subtotal = item.productInfo.price * item.quantity;
    } else {
      this.cart[product.id] = {
        productInfo: product,
        quantity: quantity,
        subtotal: product.price * quantity,
      }
    }

    this.localStorageSetCart();
  }

  deleteItem(id: number) {
    this.cart = this.localStorageGetCart();
    delete this.cart[id];
    this.localStorageSetCart();
  }

  clearCart() {
    this.cart = {};
    this.localStorageSetCart();
  }

  getItemsQuantity(): number {
    return Object.entries(this.cart).length;
  }
}
