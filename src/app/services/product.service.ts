import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { Observable} from 'rxjs';
import { tap } from 'rxjs/operators';

import { IProduct } from '../models/product';

@Injectable({
  providedIn: 'root'
})
export class ProductService {
  private apiUrl = 'api/products';

  constructor(
    private http: HttpClient,
  ) {}

  getProducts(slug?: string | undefined): Observable<IProduct[]> {
    const url: string = this.apiUrl + (slug ? `?category=${slug}` : '');
    
    return this.http.get<IProduct[]>(url)
      .pipe(
        tap(() => console.log('Get products')),
      );
  }

  getProduct(id: number): Observable<IProduct> {
    const url: string = `${this.apiUrl}/${id}`;

    return this.http.get<IProduct>(url)
      .pipe(
        tap(() => console.log('Get single product')),
      );
  }
}
