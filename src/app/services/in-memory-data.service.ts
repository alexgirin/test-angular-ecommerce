import { Injectable } from '@angular/core';

import { InMemoryDbService } from 'angular-in-memory-web-api';

import { CATEGORIES } from '../models/mock/mock-categories';
import { ICategory } from '../models/category';
import { IOrder } from '../models/order';
import { IProduct } from '../models/product';
import { PRODUCTS } from '../models/mock/mock-products'

@Injectable({
  providedIn: 'root'
})
export class InMemoryDataService implements InMemoryDbService {
  createDb() {
    const categories: ICategory[] = CATEGORIES;
    const orders: IOrder[] = [];
    const products: IProduct[] = PRODUCTS;

    return {
      categories,
      orders,
      products,
    };
  }

  genId(orders: IOrder[]): number {
    return orders.length > 0 ? Math.max(...orders.map(order => order.id)) + 1 : 1;
  }
}
