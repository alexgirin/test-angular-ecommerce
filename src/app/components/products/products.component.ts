import { Component, OnInit } from '@angular/core';

import { CartService } from 'src/app/services/cart.service';
import { IProduct } from 'src/app/models/product';
import { ProductService } from 'src/app/services/product.service';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css']
})
export class ProductsComponent implements OnInit {
  products: IProduct[] = [];
  filteredProducts: IProduct[] = [];

  constructor(
    private productService: ProductService,
    private cartService: CartService,
  ) {}

  ngOnInit() {
    this.getProducts();
  }

  getProducts() {
    this.productService.getProducts()
        .subscribe((products) => {
          this.products = products;
          this.filteredProducts = products;
        });
  }

  getProductsByCategorySlug(slug?: string) {
    if (slug) {
      this.filteredProducts = this.products.filter((product) => {
        return product.categories.includes(slug);
      });
    } else {
      this.filteredProducts = this.products;
    }
  }

  addToCart(product: IProduct) {
    this.cartService.addItem(product, 1);
  }
}
