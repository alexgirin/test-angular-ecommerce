import { Component, EventEmitter, OnInit, Output } from '@angular/core';

import { CategoryService } from 'src/app/services/category.service';
import { ICategory } from 'src/app/models/category';

@Component({
  selector: 'app-categories',
  templateUrl: './categories.component.html',
  styleUrls: ['./categories.component.css']
})
export class CategoriesComponent implements OnInit {
  @Output() readonly categorySelected = new EventEmitter<string>();
  categories: ICategory[] = [];

  constructor(
    private categoryService: CategoryService,
  ) {}

  ngOnInit() {
    this.getCategories();
  }

  getCategories() {
    this.categoryService.getCategories()
      .subscribe((categories) => this.categories = categories);
  }

  selectCategory(slug?: string) {
    this.categorySelected.emit(slug);
  }
}
