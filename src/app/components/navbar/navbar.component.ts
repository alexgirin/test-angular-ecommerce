import { Component, OnInit } from '@angular/core';
import { CartService } from 'src/app/services/cart.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {
  private itemsQuantity: number = 0;

  constructor(
    private cartService: CartService,
  ) {}

  ngOnInit() {}

  getCartQuantity() {
    this.itemsQuantity = this.cartService.getItemsQuantity();
    
    return this.itemsQuantity; 
  }
}
