import { Component, OnInit, EventEmitter, Output } from '@angular/core';

import { CartService } from 'src/app/services/cart.service'
import { ICartItem } from 'src/app/models/cart';;

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.css']
})
export class CartComponent implements OnInit {
  cart: ICartItem[] = [];

  constructor(
    private cartService: CartService,
  ) {}

  ngOnInit() {
    this.getItemsInCart();
  }

  getItemsInCart() {
    this.cart = this.cartService.getItems()
  }

  deleteFromCart(productId: number) {
    this.cartService.deleteItem(productId);
    this.getItemsInCart();
  }

  getSubtotalPrice(): number {
    let subtotal: number = 0;

    for (let item of this.cart) {
      subtotal += item.subtotal;
    }

    return subtotal;
  }

  clearCart() {
    this.cartService.clearCart();
    this.getItemsInCart();
  }
}
