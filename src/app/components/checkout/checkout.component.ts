import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';

import { CartService } from 'src/app/services/cart.service';
import { CheckoutService } from 'src/app/services/checkout.service';
import { ICartItem } from 'src/app/models/cart';
import { IOrder } from 'src/app/models/order';

@Component({
  selector: 'app-checkout',
  templateUrl: './checkout.component.html',
  styleUrls: ['./checkout.component.css']
})
export class CheckoutComponent implements OnInit {
  cart: ICartItem[] = [];
  orderForm: FormGroup;
  firstNameCtrl: FormControl;
  lastNameCtrl: FormControl;
  emailCtrl: FormControl;

  constructor(
    private cartService: CartService,
    private checkoutService: CheckoutService,
    private router: Router,
    private fb: FormBuilder,
  ) {
    this.firstNameCtrl = fb.control('', [ Validators.required ]);
    this.lastNameCtrl = fb.control('', [ Validators.required ]);
    this.emailCtrl = fb.control('', [ Validators.required, Validators.email]);

    this.orderForm = fb.group({
      firstName: this.firstNameCtrl,
      lastName: this.lastNameCtrl,
      email: this.emailCtrl,
    })
  }

  ngOnInit() {
    this.cart = this.getCartItems()
  }

  getCartItems() {
    return this.cartService.getItems();
  }

  getCartTotalPrice(): number {
    let subtotal: number = 0;

    for (let item of this.cart) {
      subtotal += item.subtotal;
    }

    return subtotal;
  }

  getCartTotalQuantity(): number {
    return this.cartService.getItemsQuantity();
  }

  submitOrder() {
    let order: IOrder = this.orderForm.value;
    order.cart = this.cart;
    
    this.checkoutService.saveOrder(order).subscribe();
    
    this.cartService.clearCart();
    this.router.navigate(['']);
  }
}
