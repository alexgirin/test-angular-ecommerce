import { ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';

import { CartService } from 'src/app/services/cart.service';
import { IProduct } from 'src/app/models/product';
import { ProductService } from 'src/app/services/product.service';

@Component({
  selector: 'app-product-detail',
  templateUrl: './product-detail.component.html',
  styleUrls: ['./product-detail.component.css']
})
export class ProductDetailComponent implements OnInit {
  product: IProduct | undefined;

  constructor(
    private location: Location,
    private route: ActivatedRoute,
    private productService: ProductService,
    private cartService: CartService,
  ) {}

  ngOnInit() {
    this.getProduct()
  }

  getProduct() {
    const id: number = Number(this.route.snapshot.paramMap.get('id'));
    this.productService.getProduct(id)
      .subscribe((product) => this.product = product);
  }

  addToCart(product: IProduct) {
    this.cartService.addItem(product, 1);
  }

  goBack() {
    this.location.back();
  }
}
