export interface ICategory {
  id: number,
  title: string,
  slug: string,
}
