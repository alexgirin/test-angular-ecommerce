import { ICartItem } from "./cart";

export interface IOrder {
  id: number,
  firstName: string,
  lastName: string,
  email: string,
  cart: ICartItem[],
}