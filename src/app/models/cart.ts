import { IProduct } from "./product";

export interface ICartItem {
  productInfo: IProduct,
  quantity: number,
  subtotal: number,
}