import { ICategory } from "../category";

export const CATEGORIES: ICategory[] = [
  {
    id: 1,
    title: 'Smartphones',
    slug: 'smartphones',
  },
  {
    id: 2,
    title: 'Laptops',
    slug: 'laptops',
  },
  {
    id: 3,
    title: 'Printers',
    slug: 'printers',
  },
] 