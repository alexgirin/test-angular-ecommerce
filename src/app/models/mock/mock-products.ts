import { IProduct } from "../product";

export const PRODUCTS: IProduct[] = [
  {
    id: 1,
    categories: ['smartphones'],
    title: 'Iphone 11',
    img: 'https://placeimg.com/400/400/tech',
    description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.', 
    price: 399.99
  },
  {
    id: 2,
    categories: ['smartphones'],
    title: 'Iphone 12',
    img: 'https://placeimg.com/400/400/tech',
    description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.', 
    price: 499.99
  },
  {
    id: 3, 
    categories: ['laptops'],
    title: 'Lenovo IdeaPad 3',
    img: 'https://placeimg.com/400/400/tech',
    description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.', 
    price: 599.99
  },
  {
    id: 4, 
    categories: ['laptops'],
    title: 'HP Pavilion 15',
    img: 'https://placeimg.com/400/400/tech',
    description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.', 
    price: 699.90
  },
  {
    id: 5,
    categories: ['printers'],
    title: 'Canon PIXMA TR4520',
    img: 'https://placeimg.com/400/400/tech',
    description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.', 
    price: 799.00
  },
  {
    id: 6,
    categories: ['laptops'],
    title: 'Lenovo Legion 5',
    img: 'https://placeimg.com/400/400/tech',
    description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.', 
    price: 1399.00
  },
  {
    id: 7,
    categories: ['printers'],
    title: 'Canon TR8620',
    img: 'https://placeimg.com/400/400/tech',
    description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.', 
    price: 799.00
  },
  {
    id: 8,
    categories: ['smartphones'],
    title: 'Google Pixel 4a',
    img: 'https://placeimg.com/400/400/tech',
    description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.', 
    price: 399.00
  },
];