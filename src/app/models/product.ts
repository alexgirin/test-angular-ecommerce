export interface IProduct {
    id: number,
    categories: string[],
    img: string,
    title: string,
    description: string,
    price: number,
}